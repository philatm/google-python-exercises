#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them
def list_of_special(directory):
    list_files = []
    filenames = os.listdir(directory)
    for filename in filenames:
        found = re.search(r'__\w+__', filename)
        if found:
            path = os.path.join(directory, filename)
            path = os.path.abspath(path)
            list_files.append(path)
    return list_files
    
def copy_special(source_path, destination_directory):
    (head, tail) = os.path.split(source_path)
    destination_path = os.path.join(destination_directory, tail)
    shutil.copy(source_path, destination_path)
    return
            

def print_list(somelist):
    for string in somelist:
        print string

def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)
  
  # +++your code here+++
  # Call your functions
  if not todir and not tozip:
      for arg in args:
          somelist = list_of_special(arg)
          print_list(somelist)
  if todir:
      for arg in args:
          somelist = list_of_special(arg)
          for path in somelist:
              copy_special(path, todir)
  if tozip:
      for arg in args:
          somelist = list_of_special(arg)
          cmd = ''
          for string in somelist:
              cmd += ' ' + string
          cmd = 'zip -j ' + tozip + cmd
          print 'I want to do ', cmd
          (status, output) = commands.getstatusoutput(cmd)
          
      
          
if __name__ == "__main__":
  main()
