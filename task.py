#!/usr/bin/python -tt
"""This task to create two dimensional dict,
	which will be symetric"""
def task(tup):
	dictionary = {}
	for i in tup:
		dictionary[i[0]] = dictionary.get(i[0], {})
		dictionary[i[0]][i[1]] = i[2]
		dictionary[i[1]] = dictionary.get(i[1], {})
		dictionary[i[1]][i[0]] = i[2]
	return dictionary

def main():
	res = task((('Oslo', 'Chita', 234),('London', 'Astana', 743),))
		
if __name__ == '__main__':
  main()
